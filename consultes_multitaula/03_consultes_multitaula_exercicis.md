# Consultes multitaula



## Exercici 1

Llista la ciutat de les oficines, i el nom i títol dels directors de cada oficina.



## Exercici 2

Llista totes les comandes mostrant el seu número, import, número de client i límit de crèdit.



## Exercici 3

Llista el número de totes les comandes amb la descripció del producte demanat.



## Exercici 4

Llista el nom de tots els clients amb el nom del representant de vendes assignat.



## Exercici 5

Llista la data de totes les comandes amb el numero i nom del client de la comanda.



## Exercici 6

Llista les oficines, noms i títols del seus directors amb un objectiu superior a 600.000.



## Exercici 7

Llista els venedors de les oficines de la regió est.



## Exercici 8

Llista les comandes superiors a 25000, incloent el nom del venedor que va servir la comanda i el nom del client que el va sol·licitar.



## Exercici 9

Llista les comandes superiors a 25000, mostrant el client que va servir la comanda i el nom del venedor que té assignat el client.



## Exercici 10

Llista les comandes superiors a 25000, mostrant el nom del client que el va ordenar, el venedor associat al client, i l'oficina on el venedor treballa.



## Exercici 11

Llista totes les combinacions de venedors i oficines on la quota del venedor és superior a l'objectiu de l'oficina.



## Exercici 12

Informa sobre tots els venedors i les oficines en les que treballen.



## Exercici 13

Llista els venedors amb una quota superior a la dels seus directors.



## Exercici 14

Llistar el nom de l'empresa i totes les comandes fetes pel client 2103.



## Exercici 15

Llista aquelles comandes que el seu import sigui superior a 10000, mostrant el numero de comanda, els imports i les descripcions del producte.



## Exercici 16

Llista les comandes superiors a 25000, mostrant el nom del client que la va demanar, el venedor associat al client, i l'oficina on el venedor treballa. També cal mostar la descripció del producte.



## Exercici 17

Trobar totes les comandes rebudes en els dies en que un nou venedor va ser contractat. Per cada comanda mostrar un cop el número, import i data de la comanda.



## Exercici 18

Mostra el nom, les vendes dels treballadors que tenen assignada una oficina, amb la ciutat i l'objectiu de l'oficina de cada venedor.



## Exercici 19

Llista el nom de tots els venedors i el del seu director en cas de tenir-ne. El camp que conté el nom del treballador s'ha d'identificar amb "empleado" i el camp que conté el nom del director amb "director".



## Exercici 20

Llista totes les combinacions possibles de venedors i ciutats.



## Exercici 21

Per a cada venedor, mostrar el nom, les vendes i la ciutat de l'oficina en cas de tenir-ne una d'assignada.



## Exercici 22

Mostra les comandes de productes que tenen unes existències inferiors a 10. Llistar el numero de comanda, la data de la comanda, el nom del client que ha fet la comanda, identificador del fabricant i l'identificador de producte de la comanda.



## Exercici 23

Llista les 5 comandes amb un import superior. Mostrar l'identificador de la comanda, import de la comanda, preu del producte, nom del client, nom del representant de vendes que va efectuar la comanda i ciutat de l'oficina, en cas de tenir oficina assignada.



## Exercici 24

Llista les comandes que han estat preses per un representant de vendes que no és l'actual representant de vendes del client pel que s'ha realitzat la comanda. Mostrar el número de comanda, el nom del client, el nom de l'actual representant de vendes del client com a "rep_cliente" i el nom del representant de vendes que va realitzar la comanda com a "rep_pedido".



## Exercici 25

Llista les comandes amb un import superior a 5000 i també aquelles comandes realitzades per un client amb un crèdit inferior a 30000. Mostrar l'identificador de la comanda, el nom del client i el nom del representant de vendes que va prendre la comanda.

