# Programari per al control de versions: GIT


### Instal·lació del programari

Podríem comprovar si tenim instal·lat git i, si no és el cas, l'instal·lar-lo,
però optarem millor per ordenar al sistema que l'instal·li i ja s'encarregarà ell de dir-nos si ho fa o no:

```
[root@paradise ~]# apt-get install git
```


## Inicialització del repositori

Primer de tot escollim el directori del qual volem fer el seguiment (tracking)
de manera que tindrem un històric de tots els canvis que hi haurà en aquest directori.

Un cop escollit un que ja existeix, o un de nou, ens col·loquem al directori.
Tot i que no seria necessari posem aquest fitxer que esteu llegint al directori esmentat.

Posteriorment inicialitzem el repositori amb l'ordre:

```
git init
```

A data de 2021 ens surt un missatge relacionat amb la consideració de la branca
_master_ la branc principal com a nom políticament incorrecte (més informació
[aquí per
exemple](https://about.gitlab.com/blog/2021/03/10/new-git-default-branch-name/))

Ens suggereix diferents solucions, nosaltres farem servir la que posa com a nom
de branca principal 'main' per a tots els projectes que creem a partir d'aquest
moment.

Executem per tant:

```
git config --global init.defaultBranch main
```

Fixem-nos que tot i el warning al final dels missatges ens diu que ha
inicialitzat un repositori.

Podem comprovar que després d'executar l'ordre anterior ja no ens sortiran els
_warnings_ inicialitzant un directori prova2 dintre de `/tmp` per exemple.

```
mkdir /tmp/prova2
cd /tmp/prova2
git init
```

I ens fixem en el missatge que retorna la instrucció anterior.

```
"Initalized an empty git repository in /home/.../el_directori_escollit/.git"
```

El missatge és interessant: tot i que al menys hi ha un fitxer, diu que el repository git està buit.

## Mostra de l'estat del repositori

Hi ha una ordre de git que ens permet comprovar l'estat del repositori:

```
git status
```
que ens mostra:

```
...
Untracked files:
(use "git add <file>..." to include in what will be committed)

   01-primers_passos.md      # <==== o el fitxer o fitxers que hi hagi
nothing added to commit but untracked files present (use "git add" to track)
...
```


És a dir ens diu bàsicament que:

- Hi ha algun/s fitxer/s dintre del directori del/s qual/s no estem fent seguiment (tracking)
- Si volem fer el seguiment del/s fitxer/s ens dona la instrucció per fer-ho.

Abans però farem un cop d'ull a l'esquema d'un projecte git que es troba en [un altre fitxer](esquema_projecte_git.md).
