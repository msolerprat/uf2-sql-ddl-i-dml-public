# Configuració bàsica de git

## Introducció

Anem a treballar amb un programari de control de versions *cvs* (_concurrent version systems_ o _sistema de control de versions_). 

Aquest sistema ens permet tenir un registre de tot el treball realitzat i els canvis fets, de manera que facilita la recuperació de versions antigues d'aquest treball.
Encara té una utilitat més important que és la de facilitar el treball col·laboratiu, per exemple un equip de desenvolupadors,  utilitzant el concepte de branques, aquesta però és una part que no veurem en aquesta introducció.

D'altra banda no hem de pensar en aquest software com una eina exclusiva de desenvolupadors, ja que la tipologia d'usuaris de git inclou fins i tot [escriptors](https://www.penflip.com/).

## Definició
GIT és un sistema de versions distribuït, per tant podem tenir rèpliques del nostre treball en diferents ordinadors.

Quan fem un canvi en el nostre ordinador el podem distribuir a tantes màquines com vulguem per tal de tenir una còpia dels canvis.
També podem baixar-nos una còpia d'aquests canvis des d'on hi hagi alguna rèplica allà on vulguem.

Normalment, treballarem amb l' ordinador de l'escola, el Fedora del meu portàtil, el Debian del mateix portàtil, l'Ubuntu d'una torre que tinc ... 
i mantindrem una còpia del nostre treball en algun dels diferents hostings que ens ofereix el mercat. 
Aquests hostings, a més de guardar una altra còpia del nostre treball, ens ofereixen una interfície web que ens permet veure els canvis fets,
l'històric i molt més d'una manera amigable. 
Si bé és cert que si algú té algun ordinador que ofereix servei a internet 24  hores i vol instal·lar-se alguna d'aquestes aplicacions web per *git* ho pot fer (*Gitlab* és una opció).

El més popular és *Github*, ofereix repositoris gratuïts però tots públics, i si volem un privat és de pagament. 
Tot i tenir una gran quantitat de projectes de programari lliure, paradoxalment *Github* no ho és.
D'altra banda tenim *GitLab*, que sí que és programari lliure i que ens ofereix el mateix que Github i a més repositoris privats de manera gratuïta. 
Hi ha altres serveis que sí que són de pagament.


## Creació d'un compte a *Gitlab*

Anem a la pàgina de [gitlab](https://gitlab.com) i ens registrem amb username *iawxxxxx* (on *xxxxx* són els dígits del nostre compte de correu).
Un cop registrats, rebut el correu, i clicat l'enllaç que es troba dintre del correu, podrem entrar al nostre compte de Gitlab, i tindrem una pantalla com aquesta:
![pàgina de benvinguda de gitlab](Welcome_Gitlab.png)

## Transferència de dades mitjançant ssh. La clau ssh.

Hi ha diferents mètodes per transferir les dades entre diferents ordinadors. 

Escollim el mètode ssh<sup>1</sup> i per utilitzar-lo necessitem tenir una clau ssh
que ens permet establir una connexió segura entre el nostre ordinador i l'ordinador on desem la còpia,
en aquest cas *GitLab* 

Des del nostre ordinador creem una clau publica i una privada que fan parella.
Si li passem a un altre ordinador la nostra clau publica, al fer la connexió entre els dos ordinadors,
es comprova que les dues claus són parella, en cas afirmatiu hi haurà confiança,
i això permetrà la connexió segura.

Per tant, el nostre objectiu es crear aquesta parella de claus al nostre pc i pujar la clau pública a *Gitlab*.
Per fer això tenim una ajuda molt bona al mateix *Gitlab*.
A la pantalla de benvinguda, l'anterior, cliquem l'opció de la columna de l'esquerra que diu «profile settings».

Un cop s'hagi carregat aquesta nova pàgina, a la part de dalt cliquem l'opció «SSH Keys»
tal i com veieu a la pantalla de sota.

![Imatge on surt la pantalla de Profile Settings des d'on hem d'anar a SSH Keys](SSH_Keys.png)

Hi ha un missatge que us diu que abans d'afegir una clau l'heu de generar:

![Imatge on surt la pantalla SSH Keys des d'on podem anar a l'ajuda per generar les claus SSH](Generate_SSH_key.png)

L'enllaç us porta a l'ajuda esmentada. Només haureu de seguir les indicacions des d'una consola de text (terminal)

![Imatge on surt la pantalla d'ajuda per generar les claus SSH](SSH_keys_HELP.png)

Deixeu el directori que us ofereix per defecte i poseu un password.
Un cop hem creat la parella de claus seguint l'ajuda podem copiar el contingut de la clau pública
i tornar a la pàgina anterior on podrem desar aquest contingut després de clicar *Add SSH Key*.
Recordem de posar un nom descriptiu com per exemple pc-escola, portatil-fedora ... 
de manera que ens ajudi a diferenciar uns ordinadors d'altres.

![Imatge on surt la pantalla SSH Keys des d'on podem inserir la clau publica del nostre pc](Copy_SSH_Key.png)

Haurem de fer això per cada un dels ordinadors des dels quals ens connectarem.

## Instal·lació del programari *git*

Per treballar amb git necessitem instal·lar el paquet git, per tant com a root:
```
dnf install git
```

## Primer projecte de prova amb git

Per crear un projecte a Gitlab podem cercar la secció *Projects* i, un cop es carregui la pàgina,
prémer el botó **New Project** o clicar el logo
![icona, símbol suma que representa la creació d'un nou projecte](Add\_Project_Icon.png) 
que es troba a la cantonada superior dreta.

Escollim un nom per al projecte, el nom del repositori git del projecte 
no pot contenir espais en blanc, ja que serà una URI.
El nom del projecte, que es pot modificar en qualsevol moment, sí que pot tenir espais en blanc. 

Fixeu-vos que, un cop s'ha creat aquest projecte, tenim unes instruccions
per executar a la nostra línia de comandes i que l'ordre amb la qual es treballa és *git*.

Des de la terminal, i com a usuari iawxxxx, crearem un directori per fer aquest primer exemple:

```
mkdir  projectesProva
```

Canvio al directori creat:

```
cd  projectesProva
```

Configurem paràmetres com el nom i l'email:
```
git config --global user.name "josep maynou"
git config --global user.email "jmaynou@tralara.com"
```

Creem un nou repositori local (o sigui a un pc/portàtil local) i
clonem el projecte de *Gitlab* que, per cert, no conté ni un petit i trist *README.md*.
```
git clone git@gitlab.com:scarlett/my-first-project.git
```
(canvieu l'usuari i el nom del projecte de l'ordre anterior pels vostres)

Ens movem al directori que ens acaba de crear

```
cd my-first-project
```

Creem un fitxer README.md 

```
touch README.md
```

Hem fet un petit canvi, l'afegim a l'index d'elements que canviarem amb l'ordre *add*:
```
git add README.md
```

Ara podríem continuar fent més canvis poc importants ... En el moment que considerem que tenim un conjunt de canvis interessants formant una mini-versió del nostre treball llavors fem un commit amb un missatge que és obligatori i que és molt important que descrigui el que fa aquesta versió respecte a l'anterior.

Fem aquest commit:
```
git commit -m "Empty README.md added"
```

Podríem pensar que ja tenim pujada una rèplica al ordinador remot de *Gitlab*, però no és així.
Només està canviada la còpia del nostre ordinador local. De fet, l'ideal, és continuar fent noves versions (commits) i, quan considerem que ja és hora de descansar o plegar, pujar la feina a *Gitlab* amb l'ordre *push*. I serà llavors quan es *pusharà* el nostre treball.

```
git push -u origin master
```

Ara sí que si fem un cop d'ull al nostre projecte a l'ordinador remot de *Gitlab* hauria de sortir-nos aquest nou fitxer README.md

## Exercici 1

Practiqueu ara una mica fent el següent. Creareu el directori *dir1* dintre de *my-first-project* (o el nom que li hagueu posat).
Dintre de *dir1* creareu un fitxer de nom *fitxer.txt* i amb el vim afegireu 3 línies.
Aquesta serà un primera versió del document, després eliminareu la darrera línia del fitxer i aquesta serà també una altra versió del document.
Un cop s'hagin pujat els canvis a l'ordinador remot de gitlab comproveu que no només podeu veure la darrera versió sinó també l'anterior.

## Exercici 2

Anem a simular en aquest exercici que estem treballant amb dos ordinadors i a més volem tenir còpia a Gitlab mitjançant ssh.
És molt important tenir present que no estem fent treball col·laboratiu o en equip, per tant no es farà cap merge (treballarem amb una única branca: la del master)

La manera de fer la simulació serà la següent:
* Treballarem amb dos pc's: *pc\_escola* i *pc\_casa*, però no al mateix temps.
* Pensarem que un pc és el de l'escola i l'altre és el de casa. O sigui farem veure que hem arribat a casa molt ràpid.
* Hi ha d'haver 2 claus ssh (una per cada pc: «pc_escola» i «pc_casa») i totes dues pujades a Gitlab.
* No es poden fer les operacions de pujada (push) o baixada (pull) de forma des-sincronitzada: heu de simular que sou un únic usuari i per tant no podeu fer 2 coses al mateix temps.

Les operacions que fareu seran:
* Fer la configuració adient de les claus ssh.
* Crear un repositori a Gitlab per exemple git-repository-exercici.
* Al *pc\_escola* cloneu el projecte de Gitlab . Afegiu un o dos programes senzills a dins (tipus *Hello World*)
* Ara haureu de jugar amb *add* per afegir els diferents fitxers a *git* que heu modificat i *commit* amb un missatge descriptiu,
per tenir una nova versió del directori. Podeu fer tants add's i commit's com considereu necessaris. I quan penseu que és hora de «plegar» feu un push per sincronitzar la informació amb l'ordinador de Gitlab.
* Aneu a l'altre pc, *pc\_casa*, baixeu-vos el projecte.
* Afegiu un altre fitxer amb un programa senzill.
* Feu les proves que considereu convenients usant els tres ordinadors (els dos de l'aula i el de Gitlab).


## Esquema projecte GIT

Un projecte GIT el podem pensar de la següent manera:

![Imatge on surt l'esquema dels tres elements més importants en un projecte GIT](EsquemaGit.png)


* Un *directori de treball*: a on farem tot el treball editant, creant, esborrant i organitzant els fitxers.
* Una *àrea temporal*:  a on s'inclouran els canvis fets al directori de treball
* Un *repositori*: a on *Git* emmagatzemarà permanentment els canvis anteriors com a versions diferents del mateix projecte



## Extra links: 
* [The simple guide\. no deep shit](http://rogerdudler.github.io/git-guide/)
* [Manual de git fet per un professor del nostre departament](https://Gitlab.com/jordinas/gitet)
* [SSH error: Permission denied](https://help.github.com/articles/error-agent-admitted-failure-to-sign)

---

<sup>1</sup> Hi ha quatre formes de transferir dades entre el Gitlab i el vostre ordinador [més info](https://git-scm.com/book/tr/v2/Git-on-the-Server-The-Protocols) . Descartarem dos d'aquests mètodes i ens quedem amb ssh i https. Tots dos són segurs, però https no pot automatitzar els push ni els pulls i et demanarà la contrasenya cada vegada. Per a això ens quedem amb el mètode per ssh.
