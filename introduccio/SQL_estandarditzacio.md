# Interoperabilitat i estandardització de SQL


## El problema

SQL és el llenguatge de referència per gestionar dades en Sistemes Gestors de Bases de Dades Relacionals (Relational DataBase Manager System RDBMS).

Malauradament cada fabricant de RDBMS implementa un SQL amb certes variants que fan que el codi no sigui compatible sempre amb la resta de RDBMS. És a dir la interoperabilitat entre diferents RDBMS no està garantida.

Existeix un estàndard SQL que intenta resoldre aquest problema, indicant quines són les regles de joc que s'han de seguir. La majoria de RDBMS no segueixen aquests estàndards, un dels que més el segueix és PostgreSQL.

[Extret de la wikipedia](https://en.wikipedia.org/wiki/SQL#Interoperability_and_standardization)


## La solució

Escriure consultes que segueixin els estàndards SQL, sempre que el nostre RDBMS ho permeti.

Dintre del estàndard SQL existeix el que es diu el Core SQL, el que es consideren les normes obligatòries i després s'afegeixen d'altres opcionals. 

Segons diuen a la documentació de PostgreSQL segueixen 170 de 177 de les funcionalitats obligatòries de SQL:2016, o sigui del Core SQL-2016 (naturalment, en el moment d'escriure's). 

A la documentació de PostgreSQL 13 per exemple, podem trobar [una llista de les funcionalitats Core SQL que PostgreSQL suporta](https://www.postgresql.org/docs/13/features-sql-standard.html), així com [una altra llista de les funcionalitats de Core-SQL 2016 que encara no estan suportades.](https://www.postgresql.org/docs/13/unsupported-features-sql-standard.html)


## Validador de consultes SQL

Si tenim dubtes sobre si la nostra consulta SQL compleix els estàndards Core SQL-2016 sempre podem fer servir [aquest validador](https://developer.mimer.com/sql-2016-validator/)
