# Consultes resum

# Exercici 0
En una mateixa consulta, calcula la suma de les quotes, quants venedors n'hi ha, i finalment mostra la quota promig calculada a partir de les funcions anteriors i també amb una funció especial per al promig. Compara aquests dos últims resultats. Si volguéssim que la darrera funció actui com la penúltima com ho faries?

## Exercici 1
Quina és la quota promig mostrada com a "prom_quota" i la venda promig
mostrades com a "prom_vendes" dels venedors?

## Exercici 2
Quin és el promig del rendiment dels venedors (promig del percentatge de les vendes
respecte la quota)?

## Exercici 3

Quines són les quotes totals com a *t_quota* i vendes totals com a *t_vendes*
de tots els venedors?

## Exercici 4
Calcula el preu mig dels productes del fabricant amb identificador "aci".

## Exercici 5
Quines són les quotes assignades mínima i màxima?

## Exercici 6
Quina és la data de comanda més antiga?

## Exercici 7
Quin és el major percentatge de rendiment de vendes respecte les quotes de tots els venedors?
(o sigui el major percentatge de les vendes respecte a la quota)

## Exercici 8
Quants clients hi ha?

## Exercici 9
Quants venedors superen la seva quota?

## Exercici 10
Quantes comandes amb un import superior a 25000 hi ha en els registres?

## Exercici 11

Trobar l'import mitjà de les comandes, l'import total de les comandes, la
mitjana del percentatge de l'import de les comandes respecte del límit de
crèdit del client i la mitjana del percentatge de l'import de les comandes
respecte a la quota del venedor.

## Exercici 12
Compta les files que hi ha a repventas, les files del camp vendes i les del camp quota.

## Exercici 13
Demostra que la suma de restar vendes menys quota és diferent que sumar vendes i restar-li la suma de quotes.

## Exercici 14
Quants tipus de càrrecs hi ha de venedors?

## Exercici 15
Quantes oficines de vendes tenen venedors que superen les seves quotes?

## Exercici 16
De la taula clients quants clients diferents i venedors diferents hi ha.

## Exercici 17
De la taula comandes seleccionar quantes comandes diferents i clients diferents hi ha

## Exercici 18
Calcular la mitjana dels imports de les comandes.

## Exercici 19
Calcula la mitjana de l'import d'una comanda realitzada pel client amb nom d'empresa "Acme Mfg."

