# Consultes resum (PART III)

## Exercici 36

Quantes oficines tenim a cada regió?

## Exercici 37

Quants representants de vendes hi ha a cada oficina?

## Exercici 38

Quants representants de vendes té assignats cada cap de respresentants?

## Exercici 39

Per cada venedor calcular quants clients diferents ha atès ( atès = atendre una comanda)?

## Exercici 40

Quina és, per cada oficina, la suma de les quotes dels seus representants? I la mitjana de les quotes per oficina?

## Exercici 41

Quina és  la quota més alta de cada oficina?

## Exercici 42

Quants clients representa cada venedor-rep_vendes?

## Exercici 43

Quin és límit de crèdit més alt dels clients de cada venedor-repvendes?

## Exercici 44

Per cada codi de fàbrica diferents, quants productes hi ha?

## Exercici 45

Per cada id_producte diferent, a quantes fàbriques es fabrica?

## Exercici 46

Per cada nom de producte diferent, quants codis (id_fab + id_prod) tenim?

## Exercici 47

Per cada producte (id_fab + id_prod), quantes comandes tenim?

## Exercici 48

Per cada client, quantes comandes tenim? Incloure els clients que no han realitzat cap comanda

## Exercici 49

Quantes comandes ha realitzat cada representant de vendes? Incloure els venedors que no han realitzat cap comanda

## Exercici 50

Quantes comandes s'han fet, quina és la suma total dels imports d'aquestes comandes i quina és la mitjana de l'import de la comada : les comandes amb una quantitat d'entre 20 i 30 productes i de les fàbriques que continguin una 'i' o una 'a'.

## Exercici 51

De quina fàbrica s'han venut menys unitats de producte (alerta : no menys comandes, menys unitats)

## Exercici 52

Quins són els 3 venedors que han fet més comandes?

## Exercici 53

Mostra quants clients amb un nom que contingui 2 espais en blanc té assignat cada venedor.

## Exercici 54

Mostra quantes comandes ha fet cada venedor a cada client que tingui un codi múltiple de 5 o múltiple de 3 i, a més a més, que el codi de client no sigui ni el 110 ni el 102.
