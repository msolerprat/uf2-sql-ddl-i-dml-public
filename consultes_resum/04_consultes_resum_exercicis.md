# Consultes resum (PART II)

## Exercici 20

Quina és l'import promig de les comandes de cada venedor?

## Exercici 21

Quin és el rang (màxim i mínim) de quotes dels venedors per cada oficina?

## Exercici 22

Quants venedors estan asignats a cada oficina?

## Exercici 23

Per cada venedor calcular quants clients diferents ha atès ( atès = atendre una comanda)?

## Exercici 24

Calcula el total dels imports de les comandes fetes per cada client a cada
vendedor.


## Exercici 25

El mateix que a la qüestió anterior, però ordenat per client i dintre de client
per venedor.

## Exercici 26

Calcula les comandes totals per a cada venedor.

## Exercici 27

Quin és l'import promig de les comandes per cada venedor, les comandes dels
quals sumen més de 30000?

## Exercici 28

Per cada oficina amb dos o més empleats, calcular la quota total i les vendes
totals per a tots els venedors que treballen a la oficina (volem mostrar la
ciutat de l'oficina a la consulta)

## Exercici 29

Mostra el preu, l'estoc i la quantitat total de les comandes de cada producte
per als quals la quantitat total demanada està per sobre del 75% de l'estoc.

## Exercici 30

Es desitja un llistat d'identificadors de fabricants de productes. Només volem
tenir en compte els productes de preu superior a 54. Només volem que apareguin
els fabricants amb un nombre total d'unitats superior a 300.

## Exercici 31

Es desitja un llistat dels productes amb les seves descripcions, ordenat per la
suma total d'imports facturats (comandes) de cada producte de l'any 1989.

## Exercici 32

Per a cada director (de personal, no d'oficina) excepte per al gerent (el
venedor que no té director), vull saber el total de vendes dels seus
subordinats. Mostreu codi i nom dels directors.

## Exercici 33

Quins són els 5 productes que han estat venuts a més clients diferents? Mostreu
el número de clients per cada producte. A igualtat de nombre de clients es
volen ordenats per ordre decreixent d'estoc i, a igualtat d'estoc, per
descripció. Mostreu tots els camps pels quals s'ordena.

## Exercici 34

Es vol llistar el clients (codi i empresa) tals que no hagin comprat cap tipus
de Frontissa (figura a la descripció) i hagin comprat articles de més d'un
fabricant diferent.

## Exercici 35

Llisteu les oficines per ordre descendent de nombre total de clients diferents
amb comandes realitzades pels venedors d'aquella oficina, i, a igualtat de
clients, ordenat per ordre ascendent del nom del director de l'oficina. Només
s'ha de mostrar el codi i la ciutat de l'oficina.

