    1. Visualitza l'import total i el número de comandes per producte que s'han fet dels productes que tenen un codi que comneça per 4 i acaba en 2, en 3 o en 4 i dels quals s'han fet 3 o més comandes.


    2. Volem saber quants clients diferents ens han fet comandes de més de 7 unitats de productes de les fàbriques aci i rei


    3. Estem interessats en els clients diferents ens han fet comandes de més de 7 unitats de productes de les fàbriques aci i rei. Volem saber quins codis de clients ens han fet dues o més comandes d'aquestes característiques.


    4. Volem saber quin fabricant és el que té més unitats dels seus productes als nostres magatzems. Visualitza el codi de la fàbrica i el total d'unitats dels seus productes que tenim.


    5. Entre els nostres productes n'hi ha que tenen el mateix nom(descripcio). Visualitza la descripcio d'aquests productes i el preu del més car.


    6. Volem saber a quines oficines treballen venedors amb un cognom que comenci per A o per S I un nom que comenci amb T o amb S o amb B o amb N. També volem saber quants trebalaldors d'aquestes característiqes hi treballen I quina és la suma de les seves quotes per oficina. No volem que es visualitzi el Tom Snyder perquè ell no pertany a cap oficina en particular.


    7. Quins són els codis dels representants que tenen assignats més de 2 clients amb un nom d'empresa que contingui la lletra 'o' i la lletra 'i'?


    8. Quins codis de clients ens han comprat 3 o més vegades?


    9. Quins codis de clients ens han comprat més d'una vegada el mateix producte?


    10. D'entre els productes que s'han venut més d'una vegada, quin és els que ens ha donat un total d'import més baix? Visualitza el codi del producta, la quantitat de cops que s'ha venut I el total d'import que s'ha cobrat per aquest producte.
    
    
    11. Per cada venedor (o treballador o reprentant de ventes), mostrar l'identificador del venedor i un camp anomenat "preu0". El camp "preu0" ha de contenir el preu del producte més car que ha venut. 



    12. Mostrar l'identificador del fabricant i un camp anomenat "m_preu". El camp "m_preu" ha de mostrar la mitjana del preu dels productes de cada fabricant. 


    13. Per cada client que ha fet alguna compra, mostrar l'identificador del client i un camp anomenat "comandes_fetes". El camp "comandes_fetes" ha de mostrar quantes comandes ha fet cada client. 


    14. Per cada client mostrar l'identificador. Només mostrar aquells clients que la suma dels imports de les seves comandes sigui menor al limit de crèdit. 

    15. Mostrar l'identificador i la ciutat de les oficines i dos camps més, un anomenat "credit1" i l'altre "credit2". Per a cada oficina, el camp "credit1" ha de mostrar el límit de crèdit més petit d'entre tots els clients que el seu representant de vendes treballa a l'oficina. El camp "credit2" ha de ser el mateix però pel límit de crèdit més gran. 

    16. Per cada venedor i cadascun dels seus clients, mostrar l'identificador del venedor, l'identificador del client i un camp anomenat "import_m". El camp "impore_m" ha de mostrar l'import mig de les comandes que ha realitzat cada venedor a cada client diferent. 
    
    17. Per cada comanda que tenim, visualitzar la regió de l'oficina del representant de ventes que l'ha fet,  el nom del representant que l'ha fet, l'import i el nom de l'empresa que ha comprat el producte. Mostrar-ho ordenat pels 2 primers camps demanats.


    18. Mostrar la quantitat de comandes que cada representant ha fet per client i l'import total de comandes que cada venedor ha venut a cada client.  Mostrar la ciutat de l'oficina del representant de ventes, el nom del representant,  el nom del client, la quantitat de comandes representant-client i  el total de l'import de les seves comandes per client.  Ordenar resultats per ciutat, representant i client.


    19. Mostrar la quantitat de representants de ventes que hi ha a la regió Est, la quantitat de  representants de ventes que hi ha a la regió Oest i la quantitat de representants que no estan assignats a cap regió.  Només es vol comptar als representants que no tenen ciutat assignada o que tenen un 'N' minúscula o majúscula al nom de la seva ciutat  i tenen una 'm' o una 'n'  minúscula o majúscula al seu nom.


    20. Mostrar la quantitat de representants de ventes que hi ha a les diferents ciutats-oficines i la quantitat de clients que tenen associats als representants d''aquestes ciutats-oficines. Mostrar la regió, la ciutat, el número de representants i el número de clients.
    

    21.  Mostrar els noms de les empreses que han comprat productes de les fàbriques imm i rei amb un preu de catàleg (no de compra) inferior a 80 o superior a 1000. Mostrar l'empresa' el fabricant, el codi de producte, el preu, el nom del representant de ventes que ha fet la comanda i el nom del seu director.
    
    
    22. Mostrar els venedors amb un nom que no comenci ni per I ni per J que entre totes les seves ventes ens han comprat per un total superior a 25.000.


    23. Mostrar les comandes que els clients han fet a representants de ventes que no són el que tenen assignat. Mostrar l'import de la comanda, el nom del client, el nom del representant de ventes que ha fet la comanda, la seva oficina si en té, el nom del representant de ventes que el client té assignat i la seva oficina si en té.

    24. Mostrar tots els productes del catàleg de productes de l'empresa, amb tots els seus camps, ordenats de menor a major preu, el total d'unitats que se n'han venut i quants representants diferents els han venut.


    25. Per cada client mostrar el nom del representant de ventes que té assignat, el nom del seu director si en té, el nom del cap de l'oficina on treballa si té oficina i el nom del cap de l'oficina on treballa el seu director si té director i els eu director té oficina.




