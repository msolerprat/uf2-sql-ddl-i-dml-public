
# Consultes simples

Escriu la consulta SELECT que mostra el que es demana a cada exercici i la seva sortida.

## Exercici 1:

Els identificadors de les oficines amb la seva ciutat, els objectius i les vendes reals.

## Exercici 2:

Els identificadors de les oficines de la regió est amb la seva ciutat, els objectius i les vendes reals.

## Exercici 3:  

Les ciutats en ordre alfabètic de les oficines de la regió est amb els objectius i les vendes reals.

## Exercici 4:  

Les ciutats, els objectius i les vendes d'aquelles oficines que les seves vendes superin els seus objectius.

## Exercici 5:  

Nom, quota i vendes de l'empleat representant de vendes número 107.

## Exercici 6:  

Nom i data de contracte dels representants de vendes amb vendes superiors a 300000.

## Exercici 7:  

Nom dels representants de vendes dirigits per l'empleat numero 104 Bob Smith.

## Exercici 8:  

Nom dels venedors i data de contracte d'aquells que han estat contractats abans del 1988.

## Exercici 9:  

Identificador de les oficines i ciutat d'aquelles oficines que el seu objectiu és diferent a 800000.

## Exercici 10:  

Nom de l'empresa i limit de crèdit del client número 2107.

## Exercici 11:  

id_fab com a "Identificador del fabricant", id_producto com a "Identificador del producte" i descripcion com a "Descripció" dels productes.

## Exercici 12:  

Identificador del fabricant, identificador del producte i descripció del producte d'aquells productes que el seu identificador de fabricant acabi amb la lletra i.

## Exercici 13:  

Nom i identificador dels venedors que estan per sota la quota i tenen vendes inferiors a 300000.

## Exercici 14:  

Identificador i nom dels venedors que treballen a les oficines 11 o 13.

## Exercici 15:  

Identificador, descripció i preu dels productes ordenats del més car al més barat.

## Exercici 16:  

Identificador i descripció de producte amb el valor_inventario (existencies * preu).

## Exercici 17:  

Vendes de cada oficina en una sola columna i format amb format "<ciutat> té unes vendes de <vendes>", exemple "Denver te unes vendes de 186042.00".

## Exercici 18:  

Codis d'empleats que són directors d'oficines.

## Exercici 19:  

Identificador i ciutat de les oficines que tinguin ventes per sota el 80% del seu objectiu.

## Exercici 20:  

Identificador, ciutat i director de les oficines que no siguin dirigides per l'empleat 108.

## Exercici 21:  

Identificadors i noms dels venedors amb vendes entre el 80% i el 120% de llur quota.

## Exercici 22:  

Identificador, vendes i ciutat de cada oficina ordenades alfabèticament per regió i, dintre de cada regió ordenades per ciutat.

## Exercici 23:  

Llista d'oficines classificades alfabèticament per regió i, per cada regió, en ordre descendent de rendiment de vendes (vendes-objectiu).

## Exercici 24:  

Codi i nom dels tres venedors que tinguin unes vendes superiors.

## Exercici 25:  

Nom i data de contracte dels empleats que les seves vendes siguin superiors a 500000.

## Exercici 26:  

Nom i quota actual dels venedors amb el calcul d'una "nova possible quota" que serà la quota de cada venedor augmentada un 3 per cent de les seves pròpies vendes.

## Exercici 27:  

Identificador i nom de les oficines que les seves vendes estan per sota del 80% de l'objectiu.

## Exercici 28:  

Numero i import de les comandes que el seu import oscil·li entre 20000 i 29999.

## Exercici 29:  

Nom, ventes i quota dels venedors que les seves vendes no estan entre el 80% i el 120% de la seva quota.

## Exercici 30:  

Nom de l'empresa i el seu limit de crèdit de les empreses que el seu nom comença per Smith.

## Exercici 31:  

Identificador i nom dels venedors que no tenen assignada oficina.

## Exercici 32:  

Identificador i nom dels venedors, amb l'identificador de l'oficina d'aquells venedors que tenen una oficina assignada.

## Exercici 33:  

Identificador i descripció dels productes del fabricant identificat per imm dels quals hi hagin existències superiors o iguals 200, també del fabricant bic amb existències superiors o iguals a 50.

## Exercici 34:  

Identificador i nom dels venedors que treballen a les oficines 11, 12 o 22 i compleixen algun dels següents suposits:

1. han estat contractats a partir de juny del 1988 i no tenen director
2. estan per sobre la quota però tenen vendes de 600000 o menors.

## Exercici 35:  

Identificador i descripció dels productes amb un preu superior a 1000 i siguin del fabricant amb identificador rei o les existències siguin superiors a 20.

## Exercici 36:  

Identificador del fabricant,identificador i descripció dels productes fabricats pels fabricants que tenen una lletra qualsevol, una lletra 'i' i una altre lletra qualsevol com a identificador de fabricant.

## Exercici 37:  

Identificador i descripció dels productes que la seva descripció comença per "art" sense tenir en compte les majúscules i minúscules.

## Exercici 38:  

Identificador i nom dels clients que la segona lletra del nom sigui una "a" minúscula o majuscula.

## Exercici 39:  

Identificador i ciutat de les oficines que compleixen algun dels següents supòsits:

1. És de la regió est amb unes vendes inferiors a 700000.
2. És de la regió oest amb unes vendes inferiors a 600000.

## Exercici 40:  

Identificador del fabricant, identificació i descripció dels productes que compleixen tots els següents supòsits:
1. L'identificador del fabricant és "imm" o el preu és menor a 500.
2. Les existències són inferiors a 5 o el producte te l'identificador 41003.  

## Exercici 41:  

Identificador de les comandes del fabricant amb identificador "rei" amb una quantitat superior o igual a 10 o amb un import superior o igual a 10000.

## Exercici 42:  

Data de les comandes amb una quantitat superior a 20 i un import superior a 1000 dels clients 2102, 2106 i 2109.

## Exercici 43:  

Identificador dels clients que el seu nom no conté " Corp." o " Inc." amb crèdit major a 30000.

## Exercici 44:  

Identificador dels representants de vendes majors de 40 anys amb vendes inferiors a 400000.

## Exercici 45:  

Identificador dels representants de vendes menors de 35 anys amb vendes superiors a 350000.

