# Consultes simples extres

> Trobeu la consulta SQL, amb la seva sortida, que resol cada una de les
> peticions de la següent llista


## Exercici 1:

Fabricant, número i import de les comandes, l'import de les quals oscil·li
entre 10000 i 39999, i ordenades pel número de forma ascendent i pel fabricant
descendent.

## Exercici 2:

Fabricant, producte i preu dels productes on el seu identificador comenci per 4
acabi per 3 i contingui tres caracters entre mig.

## Exercici 3:

Nom i data de contracte dels empleats, les vendes dels quals, siguin superiors
a 200000 i ordenats per contracte de més nou a més vell.

## Exercici 4:

Número de comanda, data comanda, codi fabricant, codi producte i import de les
comandes realitzades entre els dies '1989-09-1' i '1989-12-31'.

Mateixa consulta però sense inlcoure les dates dels extrems.


## Exercici 5:

Identificador dels clients, el nom dels quals no conté la cadena " Mfg." o "
Inc." o " Corp." i que tingui crèdit major a 30000.

## Exercici 6:

Identificador del fabricant, identificació i descripció dels productes amb
identificador de fabricant _aci_ i els que tinguin preu menor a 500.

## Exercici 7:

Identificador de fabricant i producte i descripció dels productes del fabricant
amb identificador _aci_ amb preu superior a 1000 i els productes amb
existències que siguin superiors a 20.

## Exercici 8:

Fabricant, producte i preu dels productes amb un preu superior a 100 i unes
existèncias menors a 10 ordenat per l'identificador de producte (alfabèticament
invers) i després ordenat alfabèticament per l'identificador de fabricant.

