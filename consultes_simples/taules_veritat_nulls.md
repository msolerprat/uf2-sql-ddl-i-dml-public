# Taules de veritat (incloent NULLS)

Fem un cop d'ull a les clàssiques taules de veritat dels operadors AND i OR.

Afegim a l'esquerra l'operand NULL.


```
                     ┌────────┐                           ┌────────┐
                     │   AND  │                           │   AND  │
┌─────────┬──────────┼────────┤      ┌─────────┬──────────┼────────┤
│  false  │  false   │  false │      │  false  │  NULL    │  false │
├─────────┼──────────┼────────┤      ├─────────┼──────────┼────────┤
│  false  │  true    │  false │      │  true   │  NULL    │  NULL  │
├─────────┼──────────┼────────┤      ├─────────┼──────────┼────────┤
│  true   │  true    │  true  │      │  NULL   │  NULL    │  NULL  │
└─────────┴──────────┴────────┘      └─────────┴──────────┴────────┘



                     ┌────────┐                           ┌────────┐
                     │   OR   │                           │   OR   │
┌─────────┬──────────┼────────┤      ┌─────────┬──────────┼────────┤
│  false  │  false   │  false │      │  false  │  NULL    │  NULL  │
├─────────┼──────────┼────────┤      ├─────────┼──────────┼────────┤
│  false  │  true    │  true  │      │  true   │  NULL    │  true  │
├─────────┼──────────┼────────┤      ├─────────┼──────────┼────────┤
│  true   │  true    │  true  │      │  NULL   │  NULL    │  NULL  │
└─────────┴──────────┴────────┘      └─────────┴──────────┴────────┘
```


## Entendre el valor NULL

Una manera d'entendre els resultats de les taules quan intervé un NULL podria ser la següent:

Pensem en un `NULL` com en un valor que pot ser `true` o `false` però no sabem quin dels dos és.

Juguem amb la 1a operació:
```
false AND NULL
```

Interpretarem `NULL` com un valor desconegut que podria ser `TRUE` o `FALSE`. 

Si aquest valor desconegut fos `TRUE`:
```
false AND TRUE => false
```
Si aquest valor fos FALSE:
```
false AND FALSE => false
```

És a dir que fos quin fos el NULL ens donaria sempre `false`, el resultat final per tant està clar: `false`!


Fem el mateix ara amb l'operació:
```
true AND NULL
```

Si aquest valor desconegut fos `TRUE`:
```
true AND TRUE => true
```
Si aquest valor fos `FALSE`:
```
true AND FALSE => false
```

És a dir que el resultat de l'operació tant podria ser `true` com `false`, dependria d'aquest valor desconegut, per tant el resultat final és desconegut, incert: `NULL`.

De la mateixa manera actuaríem amb la resta d'operacions. 


## OBSERVACIÓ

Les operacions AND i OR son commutatives, de manera que un cop vist per exemple

```
true AND false
```

no estudiem
```
false AND true
```
ja que són equivalents

## SQL

Recordem que podem practicar les taules de veritat amb la nostra base de dades, per exemple a PostgreSQL podem fer:

```
SELECT true AND NULL AS logical_op; 
 logical_op 
------------
 NULL
```
